package com.endava.utmpractice.repositories;

import com.endava.utmpractice.entitites.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends CrudRepository<AppUser, Long> {

        Optional<AppUser> findById(Long id);
        List<AppUser> findAllBySurname(String surname );



}
