package com.endava.utmpractice.controllers;


import com.endava.utmpractice.entitites.AppUser;
import com.endava.utmpractice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class MainController {

    @Autowired
    UserService userService;

    @CrossOrigin
    @RequestMapping(value = "/main", produces = "application/json")
    public ResponseEntity<AppUser> doSomething(){

        AppUser appUser = userService.getUserById(2L);


        return new ResponseEntity<>(appUser,HttpStatus.OK);

    }
    @CrossOrigin
    @RequestMapping(value = "/allUsers", produces = "application/json")
    public ResponseEntity<List<AppUser>> doSomethingMore(){

        List<AppUser> appUsers= userService.getAllUserBySurname("Messi");


        return new ResponseEntity<>(appUsers ,HttpStatus.OK);

    }

}
