package com.endava.utmpractice.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class AnotherController {

    @RequestMapping(name = "/ping")
    public String doSomething(Map<String, Object> model){
        model.put("message","mymessage");
        return "welcome" ;
    }


}
