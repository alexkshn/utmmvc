package com.endava.utmpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtmpracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(UtmpracticeApplication.class, args);
    }
}
