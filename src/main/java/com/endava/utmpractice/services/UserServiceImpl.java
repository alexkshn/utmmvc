package com.endava.utmpractice.services;

import com.endava.utmpractice.entitites.AppUser;
import com.endava.utmpractice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository userRepository;

    @Override
    public AppUser getUserById(Long id) {
        Optional<AppUser> user = userRepository.findById(id);

        return user.get();
    }

    public List<AppUser> getAllUserBySurname(String surname){
        List<AppUser> users = userRepository.findAllBySurname(surname);

        return users;
    }



}
