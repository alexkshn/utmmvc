package com.endava.utmpractice.services;


import com.endava.utmpractice.entitites.AppUser;

import java.util.List;


public interface UserService {

    public AppUser getUserById(Long id);
    public List<AppUser> getAllUserBySurname(String surname);

}
